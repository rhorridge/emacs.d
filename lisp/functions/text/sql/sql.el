(defun insert-null ()
	(interactive)
	(insert "NULL")
	)

;; Quickly insert NULL
(define-key global-map (kbd "C-,") 'insert-null)

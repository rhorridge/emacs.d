(defun text/increment-number-decimal (&optional arg)
  "Increment the number forward from point by ARG."
  (interactive "p*")
  (save-excursion
    (save-match-data
      (let (inc-by field-width answer)
	(ignore-errors
	  (setq inc-by (if arg arg 1)
		(skip-chars-backward "0123456789")
		(when (re-search-forward "[0-9]+" nil t)
		  (setq field-width (- (match-end 0) (match-beginning 0)))
		  (setq answer (+ (string-to-number (match-string 0) 10) inc-by))
		  (when (< answer 0)
		    (setq answer (+ (expt 10 field-width) answer)))
		  (replace-match (format (concat "%0" (int-to-string field-width) "d")
					 answer)))))))))

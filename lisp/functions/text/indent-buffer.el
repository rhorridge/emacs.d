(defun text/indent-buffer ()
  "Indent entire buffer."
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (call-interactively 'indent-region)))

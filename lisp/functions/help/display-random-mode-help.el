(defun help/display-random-mode-help ()
	"Display random help about an enabled major or minor mode function."
	(interactive)
	(describe-mode)
	(other-window 1)
	(let ((current-prefix-arg (random 100)))
		(call-interactively 'forward-button))
	(push-button))

(defvar ytd-location "~/media/music/vid/tmp"
  "Location to place downloaded files.")

(defun ytd (url)
  (interactive "sUrl?")
  (let ((cmd (concat
              "cd "
              (shell-quote-argument ytd-location)
              "; youtube-dl -x "
              (shell-quote-argument url))))
    (princ cmd)
    (shell-command cmd)))

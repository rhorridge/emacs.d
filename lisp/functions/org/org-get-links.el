(defun org-get-links ()
  "Get links in current org-mode buffer"
  (interactive)
  (write-file
  (org-element-map (org-element-parse-buffer) 'link #'jlp/org-link-info)))

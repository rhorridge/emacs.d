(defun org-refile-create-link ()
  "Refile a subtree, creating a link to its new position."
  (interactive)
  (org-refile)
  (save-excursion
    (org-refile-goto-last-stored)
    (org-store-link)))

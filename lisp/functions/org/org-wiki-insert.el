(defun org-wiki-insert (subject)
  "Insert part of Wikipedia page best pertaining to SUBJECT into
buffer at point."
  (interactive "sSubject name? ")
  (let* ((url-head "https://en.wikipedia.org/w/index.php?search=")
	 (url-tail "&title=Special%3ASearch&fulltext=Search&ns0=1")
	 (search-term (replace-regexp-in-string " " "+" subject))
	 (url (concat url-head search-term url-tail))
	 (file-name (replace-regexp-in-string "[ |(|)|+]" "-" search-term))
	 (tmp-file-1 (concat "/tmp/" file-name ".html"))
	 (tmp-file-2 (concat "/tmp/" file-name "-target" ".html"))
	 (tmp-file-3 (concat "/tmp/" file-name ".txt"))
	 (cmd-1 (concat "wget -O '" tmp-file-1 "' '" url "'"))
	 (cmd-2 (concat
		 "sed "
		 "\"s/'mw-search-result-heading'>/\\n/g\" "
		 tmp-file-1
		 " | "
		 "grep "
		 "'^<a href=' "
		 " | "
		 "head -n 1"
		 " | "
		 "sed "
		 "'s/^<a href=\"\\(\\/wiki\\/.*\\)\" title=\".*\" data-serp-pos=.*/https:\\/\\/en.wikipedia.org\\/\\1/'")))
    (shell-command cmd-1)
    (let* ((url-target (substring (shell-command-to-string cmd-2) 0 -1))
	   (cmd-3 (concat
		   "wget -O '" tmp-file-2 "' '" url-target "'"))
	   (cmd-4 (concat
		    "sed -n "
		    "'/<p>/,/<div id=\"toc\"/p;/<div id=\"toc\"/q'"
		    " '"
		    tmp-file-2
		    "' | "
		    "sed '$d'"
		    " > '"
		    tmp-file-3
		    "'")))
      (princ cmd-3)
      (shell-command cmd-3)
      (shell-command cmd-4)
      (insert (shell-command-to-string
	       (concat "links -codepage UTF-8 -dump '"
		       tmp-file-3 "'")))
      (insert (concat "
[source: " url-target "]")))))

;concat "lynx -display_charset UTF-8 -dump '"	tmp-file-3 "'"

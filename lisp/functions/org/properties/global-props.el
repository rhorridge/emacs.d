(defun org/properties/global-props (&optional property buffer)
  "Get plists of global org properties in current buffer.

Defaults to PROPERTY \"PROPERTY\" and BUFFER (current-buffer).
https://emacs.stackexchange.com/questions/21713/how-to-get-property-values-from-org-file-headers"
  (unless property (setq property "PROPERTY"))
  (with-current-buffer (or buffer (current-buffer))
    (org-element-map (org-element-parse-buffer) 'node-property
      (lambda (elem)
	(when (string-match property (org-element-property :key elem)) elem)))))

(defun org/properties/list-props (property &optional buffer)
  "Interactively list properties in current buffer matching regular expression PROPERTY."
  (interactive "sProperty?")
  (mapcar (lambda (prop)
	    (list (org-element-property :key prop)
		  (org-element-property :value prop)))
	    (org/properties/global-props property)))

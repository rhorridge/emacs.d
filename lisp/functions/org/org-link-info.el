(defun jlp/org-link-info (link)
  "https://emacs.stackexchange.com/questions/16909/how-can-i-get-all-file-links-in-one-org-mode-file"
  (let ((path (org-element-property :path link))
	(type (org-element-property :type link))
	(desc (substring-no-properties (nth 2 link))))
    (list type path desc)))

(defun my/org/list-links ()
  "List all links in current buffer"
  (interactive)
  (let ((links (org-element-map (org-element-parse-buffer) 'link 'jlp/org-link-info))))
  (with-temp-buffer
    (links)))

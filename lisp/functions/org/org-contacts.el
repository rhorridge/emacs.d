(defun helm-org-contacts-field-insert (field)
  "Provided by tmalsburg2 at [[https://www.reddit.com/r/emacs/comments/76s9dl/orgmode_for_contacts/]]"
  (move-end-of-line nil)
  (insert (format "\n  :%s: %s" field
		  (cond
		   ((string= field "NAME") "SURNAME;GIVEN;MIDDLE;PREFIX;SUFFIX")
		   ((string= field "PHOTO") "URI")
		   ((string= field "BIRTHDAY") (format "<%s>" (format-time-string "%Y-%m-%d %a")))
		   ((string= field "ANNIVERSARY") (format "<%s>" (format-time-string "%Y-%m-%d %a")))
		   ((string= field "GENDER") "[male|female]")
		   ((string= field "LANG") "[EN|DE|FR]")
		   ((string= field "KEY") "URL")
		   ((string= field "ADDRESS") "STREET;CITY;COUNTY (WORK, pref)")
		   ((string= field "PHONE") "([MOBILE|WORK|HOME], [FAX|VOICE], pref)")
		   ((string= field "EMAIL") "([WORK|HOME], pref)")
		   ((string= field "ORG") "DEPT;INST;*")
		   ((string= field "REV") (format "[%s]" (format-time-string "%Y-%m-%d %a")))
		   (t ""))))
  (forward-line 0)
  (forward-word)
  (forward-char 2))

(setq helm-source-org-contacts-field
      '((name . "Contacts fields")
	(candidates . ("NAME" "NICKNAME" "PHOTO" "BIRTHDAY" "ANNIVERSARY"
		       "GENDER" "ADDRESS" "PHONE" "EMAIL" "IMPP" "LANG"
		       "TITLE" "ROLE" "LOGO" "ORG" "NOTE" "REV" "URL"
		       "KEY"))
	(no-matchplugin)
	(nohighlight)
	(action . helm-org-contacts-field-insert)))

(defun helm-org-contacts-field ()
  "Select a field for insertion in an org-contacts entry.
Provided by tmalsburg2 at [[https://www.reddit.com/r/emacs/comments/76s9dl/orgmode_for_contacts/]]"
  (interactive)
  (helm :sources '(helm-source-org-contacts-field)))

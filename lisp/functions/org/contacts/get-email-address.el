(defun org/contacts/insert-email-address (name)
  "Insert best match of email address matching NAME."
  (interactive "MContact name? ")
  (insert (car (org/contacts/get-email-address name))))

(defun org/contacts/get-email-address (name)
  "Find a list of email addresses matching NAME."
  (interactive "MContact name? ")
  (setq show-trailing-whitespace t)
  (save-current-buffer
    (find-file (concat home-directory "doc/personal/contact/contacts.org"))
    (let ((structure (org-element-parse-buffer 'element)))
      (let ((matching
	     (org-element-map structure 'node-property
	       #'org/contacts/get--email
	       nil t)))
	(kill-buffer "contacts.org")
	matching))))

(defun org/contacts/get--email (hl)
  "Get email address from headline HL."
  (when (string-match "NAME" (org-element-property :key hl))
    (when (string-match name (org-element-property :value hl))
      (org/contacts/get-email-from-headline
       (org-element-property :parent hl)))))

(defun org/contacts/get-email-from-headline (headline)
  "Get a matching email from HEADLINE."
  (org-element-map headline 'node-property
    (lambda (hl) (when (string-match "EMAIL"
				     (org-element-property :key hl))
		   (org-element-property :value hl)))))

(defun org-global-props (&optional property buffer)
  "Get the plists of global org properties of current buffer."
  (unless property (setq property "PROPERTY"))
  (with-current-buffer (or buffer (current-buffer))
    (org-element-map (org-element-parse-buffer) 'node-property
      (lambda (el) (when (string-match property
				       (org-element-property :key el))
		     (org-element-property :value el))))))

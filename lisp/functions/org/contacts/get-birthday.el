(defun org/contacts/get-birthday ()
  "Return an alist of (Name . Birthday) for all contacts with birthday."
  (with-current-buffer "contacts.org"
    (let ((structure (org-element-parse-buffer 'element)))
      (let ((matching (org-element-map structure 'node-property
			#'org/contacts/get--birthday)))
	matching))))

(defun orgcollect (buffer-name property-name-1 property-name-2)
  "Create an alist of (PROPERTY-NAME-1 . PROPERTY-NAME-2) from BUFFER-NAME."
  (with-current-buffer buffer-name
    (let ((structure (org-element-parse-buffer 'element)))
      (let ((matching
	     (org-element-map structure 'node-property
	       (lambda (el)
		 (if (or
		      (string-match property-name-1
				    (org-element-property :key el))
		      (string-match property-name-2
				    (org-element-property :key el))))))))))))

(defun org/contacts/get--birthday (hl)
  "Get birthday from headline HL."
  (let ((pair nil))
    (when (string-match "NAME" (org-element-property :key hl))
      (push (org-element-property :value hl) pair)
      (push (car
       (org-element-map (org-element-property :parent hl)
	  'node-property
	(lambda (hl) (when (string-match
			    "BIRTHDAY"
			    (org-element-property :key hl))
		       (org-element-property :value hl)))))
       pair))
    pair))

(org/contacts/get-birthday)

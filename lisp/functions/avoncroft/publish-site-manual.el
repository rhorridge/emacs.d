(require 'ox-publish)
(setq avoncroft-project-directory "~/ln/avc-plan-site/doc/manual/")
(setq org-publish-project-alist
      `(("org-notes"
	:base-directory ,(concat avoncroft-project-directory "org/")
	:base-extension "org"
	:publishing-directory ,(concat avoncroft-project-directory "public_html/")
	:recursive t
	:publishing-function org-html-publish-to-html
	:auto-preamble t)
      ("org-static"
       :base-directory ,(concat avoncroft-project-directory "org/")
       :base-extension "css\\|png\\|jpg\\|tif\\|gif\\|pdf\\|txt"
       :publishing-directory ,(concat avoncroft-project-directory "public_html/")
       :recursive t
       :publishing-function org-publish-attachment)
      ("org-publish"
       :components ("org-notes" "org-static"))))

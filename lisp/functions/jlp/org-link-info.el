(defun jlp/org-link-info (link)
  (let ((path (org-element-property :path link))
	(type (org-element-property :type link)))
    (list type path)))

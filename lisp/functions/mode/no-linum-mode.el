(defun no-linum-mode ()
  "Disable `linum-mode'. Intended to be used as a hook."
  (message "linum-mode deactivated.")
  (add-hook 'after-change-major-mode-hook
	    (lambda () (linum-mode -1))
	    :append :local))

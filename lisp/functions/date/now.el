(defun now ()
  "Insert string for current time."
  (interactive)
  (insert (format-time-string "%Y-%m-%d %H:%M:%S" (current-time))))

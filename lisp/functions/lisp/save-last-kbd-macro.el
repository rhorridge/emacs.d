(defvar kbd-macro-dir
  "~/.emacs.d/lisp/macros/"
  "Default directory to save keyboard macros. See `save-last-kbd-macro'.")

(setq kbd-macro-dir (concat user-emacs-directory "lisp/macros/"))

(defmacro verify--kbd-macro-symbol (symbol)
  "Verify that environment is valid for SYMBOL to be a keyboard macro name"
  `(or last-kbd-macro
      (user-error "No keyboard macro defined"))
  `(and (fboundp ,symbol)
       (not (stringp (symbol-function ,symbol)))
       (not (vectorp (symbol-function ,symbol)))
       (user-error "Function %s is already defined and not a keyboard macro"
		   ,symbol))
  `(if (string-equal ,symbol "")
      (user-error "No command name given")))

;; FIXME does not create directories
(defun save-last-kbd-macro (name)
  "Save last entered keyboard macro to `kbd-macro-dir' as NAME."
  (interactive "SMacro Name? ")
  (verify--kbd-macro-symbol name)
  (fset name last-kbd-macro)
  (with-temp-file (concat kbd-macro-dir
		     (symbol-name name)
		     ".el")
    (insert-kbd-macro name)))

(defvar user-functions-directory
  (concat user-emacs-directory "lisp/functions/")
  "Directory beneath which additional per-user Functions are placed.
See also `load-function'.")

(defun load-function (fun)
  "Load a Lisp file defined in `user-functions-directory'."
  (setq default-directory user-functions-directory)
  (interactive "fFunction: ")
  (load-file fun))

(defmacro load--function (fun)
  "Non-interactively load a Lisp file defined in `user-functions-directory'."
  `(load-file (concat ,user-functions-directory (symbol-name ,fun) ".el")))

(defvar user-kbd-macros-directory
  (concat user-emacs-directory "lisp/macros/")
  "Directory beneath which additional per-user keyboard macros are placed.
See also `load-kbd-macro'.")

(defun load-kbd-macro (macro)
  "Load a Lisp file defined in `user-kbd-macros-directory'."
  (setq default-directory user-kbd-macros-directory)
  (interactive "fKbd Macro: ")
  (load-file macro))

(defmacro load--kbd-macro (macro)
  "Non-interactively load a Lisp file defined in `user-macros-directory'."
  `(load-file (concat ,user-macros-directory (symbol-name ,macro) ".el")))

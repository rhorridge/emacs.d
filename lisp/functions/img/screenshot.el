(defvar user-screenshot-directory (concat user-emacs-directory "media/img/screenshots/")
  "Default directory for `screenshot' to save images to.")

(defun screenshot ()
  "Take a screenshot using the `import' command,
saving it to `user-screenshot-directory'."
  (interactive)
  (progn
    (take--screenshot)
    (insert (call-interactively 'save--screenshot))))

(defun take--screenshot ()
  "Take a screenshot."
  (call-process "import" nil t nil "/tmp/screenshot.png"))

(defun save--screenshot (dst)
  "Save a screenshot."
  (interactive "sName: ")
  (let ((outfile (concat user-screenshot-directory dst ".png")))
    (copy-file "/tmp/screenshot.png" outfile 1)
    (print outfile)))

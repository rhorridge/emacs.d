#+TITLE:  Emacs.D
#+AUTHOR: Richard Horridge
#+EMAIL:  rhorridge@hotmail.co.uk
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="~/.local/share/css/rhorridge.css">
#+LATEX_CLASS: org-article

* Introduction

  This repository contains initialisation files and additional functionality for
  [[https://www.gnu.org/software/emacs/][GNU Emacs]], a text editor.

* Building

  Clone or download the repository to your local system and copy the
  files into your ~~/.emacs.d~ directory:

  #+NAME: build
  #+BEGIN_SRC bash
  git clone https://gitlab.com/rhorridge/emacs.d;
  cp -ri emacs.d/* ${HOME}/.emacs.d/ # This WILL ask before overwriting existing files!
  #+END_SRC

* License

  This project is licensed under the GNU General Public License
  version 3.
